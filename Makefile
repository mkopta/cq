CC=clang
CFLAGS=-Wall -Wextra -pedantic -std=c99

all: cq test

cq: cq.c
	$(CC) $(CFLAGS) -o cq cq.c

test: cq
	./cq

clean:
	rm -f cq
