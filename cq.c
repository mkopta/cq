#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

struct cqueue {
    unsigned int size;
    unsigned int start;
    unsigned int end;
    bool is_full;
    double *numbers;
};

struct cqueue *cqueue_create(unsigned int size) {
    struct cqueue *cq = malloc(sizeof(struct cqueue));
    if (!cq) {
        perror("malloc");
        return NULL;
    }
    cq->numbers = malloc(size * sizeof(double));
    if (!cq->numbers) {
        perror("malloc");
        free(cq);
        return NULL;
    }
    cq->size = size;
    cq->start = 0;
    cq->end = 0;
    cq->is_full = false;
    return cq;
}

void cqueue_destroy(struct cqueue *cq) {
    if (cq) {
        free(cq->numbers);
        free(cq);
    }
}

bool cqueue_full(struct cqueue *cq) {
    return cq->is_full;
}

bool cqueue_empty(struct cqueue *cq) {
    return (bool) (cq->start == cq->end && !cq->is_full);
}

bool cqueue_enqueue(struct cqueue *cq, double n) {
    if (cq->is_full)
        return false;
    cq->numbers[cq->end] = n;
    cq->end = (cq->end + 1) % cq->size;
    if (cq->start == cq->end)
        cq->is_full = true;
    return true;
}

bool cqueue_dequeue(struct cqueue *cq, double *n) {
    if (cqueue_empty(cq))
        return false;
    *n = cq->numbers[cq->start];
    cq->start = (cq->start + 1) % cq->size;
    if (cq->is_full)
        cq->is_full = false;
    return true;
}

void cqueue_print(struct cqueue *cq) {
    unsigned int i = cq->start;
    if (cq->start == cq->end && !cq->is_full) {
        printf("Empty\n");
        return;
    }
    do  {
        printf("%.2f ", cq->numbers[i]);
        i = (i + 1) % cq->size;
    } while (i != cq->end);
    printf("\n");
}

int main() {
    double n;
    struct cqueue *cq = cqueue_create(4);
    assert(cqueue_full(cq) == false);
    assert(cqueue_empty(cq) == true);
    assert(cqueue_dequeue(cq, &n) == false);
    assert(cqueue_enqueue(cq, 0.0) == true);
    assert(cqueue_dequeue(cq, &n) == true);
    assert(n == 0.0);
    assert(cqueue_enqueue(cq, 0.0) == true);
    assert(cqueue_full(cq) == false);
    assert(cqueue_empty(cq) == false);
    assert(cqueue_enqueue(cq, 1.0) == true);
    assert(cqueue_enqueue(cq, 2.0) == true);
    assert(cqueue_enqueue(cq, 3.0) == true);
    assert(cqueue_full(cq) == true);
    assert(cqueue_empty(cq) == false);
    assert(cqueue_enqueue(cq, 4.0) == false);
    assert(cqueue_dequeue(cq, &n) == true);
    assert(n == 0.0);
    assert(cqueue_dequeue(cq, &n) == true);
    assert(n == 1.0);
    assert(cqueue_dequeue(cq, &n) == true);
    assert(n == 2.0);
    assert(cqueue_dequeue(cq, &n) == true);
    assert(n == 3.0);
    assert(cqueue_dequeue(cq, &n) == false);
    assert(cqueue_full(cq) == false);
    assert(cqueue_empty(cq) == true);
    cqueue_destroy(cq);
    printf("Done\n");
    return 0;
}
